#1/bin/bash
./cpu < input > results/cpu &
c_pid=$!

for ((i = 40; i <= 1024; i = i+i))
do
    for ((j = 1; j <= 4096; j = j+j))
    do
        echo $i $j
        ./gmem 0 $i $j < input > "results/gmem_${i}_${j}" &
        pid=$!
        ./shmem 1 $i $j < input > "results/shmem_${i}_${j}"
        wait $pid
    done
done

wait $c_pid

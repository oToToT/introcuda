#include <cstdio>
#include <cmath>
#include <random>
#include <cuda_runtime.h>

__global__ void calc_hist(
    const float *data, const int n,
    int *hist, const int m,
    const float Rmin, const float binsize) {

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    const int step = blockDim.x * gridDim.x;

    for (; i < n; i += step) {
        int idx = static_cast<int>((data[i] - Rmin) / binsize);
        atomicAdd(&hist[idx], 1);
    }
}

int main(int argc, char *argv[]) {
    if (argc < 4) {
        printf("Usage: %s GPU_ID BLOCK_SIZE GRID_SIZE\n", argv[0]);
        exit(0);
    }
    int gid = atoi(argv[1]);
    if (cudaSetDevice(gid) != cudaSuccess) {
        printf("Error using device %d\n", gid);
        exit(0);
    }
    printf("Using devie %d\n", gid);

    int threadsPerBlock = atoi(argv[2]);
    if (threadsPerBlock > 1024) {
        puts("The block size must be less than 1024!");
        exit(0);
    }
    printf("The number of threads is %d\n", threadsPerBlock);

    int blocksPerGrid = atoi(argv[3]);
    printf("The number of blocks is %d\n", blocksPerGrid);


    puts("Enter the size of the data vector:");
    int n; scanf("%d", &n);
    printf("%d\n", n);

    puts("Enter the data range [Rmin, Rmax] for the histogram:");
    float Rmin, Rmax;
    scanf("%f %f",&Rmin, &Rmax);
    printf("%f %f\n",Rmin, Rmax);

    puts("Enter the number of bins of the histogram:");
    int bins; scanf("%d",&bins);
    printf("%d\n",bins);

    float binsize = (Rmax - Rmin) / bins;

    float *data = (float *)malloc(n * sizeof(float));
    int *hist = (int *)malloc(bins * sizeof(int));
    memset(hist, 0, bins * sizeof(int));

    std::mt19937 rnd(7122);
    std::uniform_real_distribution<float> uni(0, 1);
    for (int i = 0; i < n; ++i)
        data[i] = -log(1 - uni(rnd));

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // copy data
    cudaEventRecord(start, 0);

    int *hist_d;
    float *data_d;
    cudaMalloc((void**)&data_d, n * sizeof(int));
    cudaMalloc((void**)&hist_d, bins * sizeof(float));


    cudaMemcpy(data_d, data, n * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(hist_d, hist, bins * sizeof(int), cudaMemcpyHostToDevice);

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float Intime;
    cudaEventElapsedTime(&Intime, start, stop);
    printf("Input time for GPU: %f (ms) \n", Intime);

    // calc data
    cudaEventRecord(start,0);

    calc_hist<<<blocksPerGrid, threadsPerBlock>>>(data_d, n, hist_d, bins, Rmin, binsize);

    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);

    float gputime;
    cudaEventElapsedTime(&gputime, start, stop);
    printf("Processing time for GPU: %f (ms) \n", gputime);

    // copy data
    cudaEventRecord(start,0);

    cudaMemcpy(hist, hist_d, bins * sizeof(int), cudaMemcpyDeviceToHost);

    cudaFree(data_d);
    cudaFree(hist_d);

    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);

    float Outime;
    cudaEventElapsedTime(&Outime, start, stop);
    printf("Output time for GPU: %f (ms)\n",Outime);

    float gputime_tot;
    gputime_tot = Intime + gputime + Outime;
    printf("Total time for GPU: %f (ms) \n",gputime_tot);

    printf("Histogram:\n");
    for (int i=0; i<bins; i++) {
        float x = Rmin + (i + 0.5) * binsize;
        printf("%.5f %d \n", x, hist[i]);
    }

    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    free(data);
    free(hist);

    cudaDeviceReset();
    return 0;
}

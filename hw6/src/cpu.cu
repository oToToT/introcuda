#include <cstdio>
#include <cmath>
#include <random>
#include <cuda_runtime.h>

int main(int argc, char *argv[]) {
    puts("Enter the size of the data vector:");
    int n; scanf("%d", &n);
    printf("%d\n", n);

    puts("Enter the data range [Rmin, Rmax] for the histogram:");
    float Rmin, Rmax;
    scanf("%f %f",&Rmin, &Rmax);
    printf("%f %f\n",Rmin, Rmax);

    puts("Enter the number of bins of the histogram:");
    int bins; scanf("%d",&bins);
    printf("%d\n",bins);

    float binsize = (Rmax - Rmin) / bins;

    float *data = static_cast<float *>(malloc(n * sizeof(float)));
    int *hist = static_cast<int *>(malloc(bins * sizeof(int)));
    memset(hist, 0, bins * sizeof(int));

    std::mt19937 rnd(7122);
    std::uniform_real_distribution<float> uni(0, 1);
    for (int i = 0; i < n; ++i)
        data[i] = -log(1 - uni(rnd));

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start, 0);

    for (int i = 0; i < n; ++i) {
        int idx = static_cast<int>((data[i] - Rmin) / binsize);
        if (0 <= idx and idx < bins)
            hist[idx]++;
    }
    
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    float Ctime;
    cudaEventElapsedTime(&Ctime, start, stop);
    printf("Processing time for CPU: %f (ms)\n", Ctime);


    printf("Histogram:\n");
    for (int i=0; i<bins; i++) {
        float x = Rmin + (i + 0.5) * binsize;
        printf("%.5f %d \n", x, hist[i]);
    }


    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    free(data);
    free(hist);
    return 0;
}

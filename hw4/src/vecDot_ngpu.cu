#include <stdio.h>
#include <omp.h>

__global__ void VecDot(const float a[], const float b[], float c[], int n) {
    extern __shared__ float cache[];
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    const int cacheIdx = threadIdx.x;

    float temp = 0;
    while (i < n) {
        temp += a[i] * b[i];
        i += ((size_t)blockDim.x) * gridDim.x;
    }
    cache[cacheIdx] = temp;
    for (int step = blockDim.x >> 1; step; step >>= 1) {
        __syncthreads();
        if (cacheIdx < step) {
            cache[cacheIdx] += cache[cacheIdx + step];
        }
    }
    if (cacheIdx == 0) {
        c[blockIdx.x] = cache[0];
    }
}

void RandomInit(float[], int);

int main() {
    int NGPU;
    printf("Enter the number of GPUs: ");
    scanf("%d", &NGPU);
    printf("%d\n", NGPU);

    int *Devices = NULL;
    Devices = (int*)malloc(sizeof(int) * NGPU);

    int numDev = 0;
    printf("GPU device number: ");
    while (numDev < NGPU) {
        scanf("%d", &Devices[numDev]);
        printf("%d ",Devices[numDev++]);
        if(getchar() == '\n') break;
    }
    printf("\n");
    if(numDev != NGPU) {
        fprintf(stderr,"Should input %d GPU device numbers\n", NGPU);
        exit(1);
    }

    printf("Vector Dot Product: A.B\n");
    int N;
    printf("Enter the size of the vectors: ");
    scanf("%d", &N);
    printf("%d\n", N);

    // Set the sizes of threads and blocks
    int threadsPerBlock;
    printf("Enter the number (2^m) of threads per block: ");
    scanf("%d", &threadsPerBlock);
    printf("%d\n", threadsPerBlock);
    if (threadsPerBlock > 1024) {
        fprintf(stderr, "The number of threads per block must be less than 1024 ! \n");
        exit(1);
    }
    if (threadsPerBlock != (threadsPerBlock & (-threadsPerBlock))) {
        fprintf(stderr, "The number of threads per block must be power of 2 1 \n");
        exit(1);
    }
    int blocksPerGrid;
    printf("Enter the number of blocks per grid: ");
    scanf("%d",&blocksPerGrid);
    printf("%d\n",blocksPerGrid);
    printf("The number of blocks is %d\n", blocksPerGrid);
    if (blocksPerGrid > 2147483647) {
        fprintf(stderr, "The number of blocks must be less than 2147483647 ! \n");
        exit(1);
    }

    const size_t size = N * sizeof(float);
    const size_t sb = blocksPerGrid * sizeof(float);
    const size_t sm = threadsPerBlock * sizeof(float);

    float *h_A = NULL, *h_B = NULL;
    double h_G = 0;
    // Allocate input vectors h_A and h_B in host memory
    h_A = (float*)malloc(size);
    h_B = (float*)malloc(size);

    RandomInit(h_A, N);
    RandomInit(h_B, N);

    cudaSetDevice(Devices[0]);
    float Intime, Outime, gputime;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    #pragma omp parallel num_threads(NGPU)
    {
        
        int cpu_thread_id = omp_get_thread_num();
        cudaSetDevice(Devices[cpu_thread_id]);

        float *h_C = NULL;
        #pragma omp critical
        h_C = (float*)malloc(sb);

        if (cpu_thread_id == 0) {
            cudaEventRecord(start, 0);
        }
        float *d_A, *d_B, *d_C;
        // Allocate vectors in device memory
        #pragma omp critical
        {
            cudaMalloc((void**)&d_A, size/NGPU);
            cudaMalloc((void**)&d_B, size/NGPU);
            cudaMalloc((void**)&d_C, sb);
        }

        cudaMemcpy(d_A, h_A + N/NGPU*cpu_thread_id, size/NGPU, cudaMemcpyHostToDevice);
        cudaMemcpy(d_B, h_B + N/NGPU*cpu_thread_id, size/NGPU, cudaMemcpyHostToDevice);
        if (cpu_thread_id == 0) {
            cudaEventRecord(stop, 0);
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&Intime, start, stop);
            printf("Data input time for GPU: %f (ms) \n", Intime);
        }

        if (cpu_thread_id == 0) {
            cudaEventRecord(start, 0);
        }
        VecDot<<<blocksPerGrid, threadsPerBlock, sm>>>(d_A, d_B, d_C, N/NGPU);
        cudaDeviceSynchronize();
        if (cpu_thread_id == 0) {
            cudaEventRecord(stop, 0);
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&gputime, start, stop);
            printf("Processing time for GPU: %f (ms) \n",gputime);
            printf("GPU Gflops: %f\n",2 * N / (1000000.0 * gputime));
    	}

        if (cpu_thread_id == 0) {
            cudaEventRecord(start, 0);
        }
        cudaMemcpy(h_C, d_C, sb, cudaMemcpyDeviceToHost);
        
        #pragma omp critical
        {
            cudaFree(d_A);
            cudaFree(d_B);
            cudaFree(d_C);
        }

        for (int i = 0; i < blocksPerGrid; i++) {
            #pragma omp atomic
            h_G += h_C[i];
        }
        if (cpu_thread_id == 0) {
            cudaEventRecord(stop, 0);
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&Outime, start, stop);
            printf("Data output time for GPU: %f (ms) \n", Outime);
        }
        #pragma omp critical
        free(h_C);
    }

    #pragma omp barrier
    float gputime_tot = Intime + gputime + Outime;
    printf("Total time for GPU: %f (ms) \n", gputime_tot);

    cudaEventRecord(start, 0);
    double h_D = 0.0;
    for (int i = 0; i < N; i++) 
        h_D += h_A[i] * h_B[i];
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    float cputime;
    cudaEventElapsedTime(&cputime, start, stop);
    printf("Processing time for CPU: %f (ms) \n", cputime);
    printf("CPU Gflops: %f\n",2 * N / (1000000.0*cputime));
    printf("Speed up of GPU = %f\n", cputime / gputime_tot);

    printf("Check result:\n");
    float diff = abs(h_D - h_G) / h_D;
    printf("|(h_G - h_D)/h_D| = %20.15e\n",diff);
    printf("h_G = %20.15e\n",h_G);
    printf("h_D = %20.15e\n",h_D);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    free(h_A);
    free(h_B);
    #pragma omp parallel num_threads(NGPU)
    {
        int cpu_thread_id = omp_get_thread_num();
        cudaSetDevice(Devices[cpu_thread_id]);
        cudaDeviceReset();
    }
    return 0;
}

void RandomInit(float arr[], int n) {
    for (int i = 0; i < n; ++i)
        arr[i] = 2. * rand() / RAND_MAX - 1.0;
}

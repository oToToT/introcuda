// Matrix addition: C = A + B.
// compile with the following command:
//
// (for GTX970)
// nvcc -arch=compute_52 -code=sm_52,sm_52 -O2 -m64 -o vecAdd vecAdd.cu
//
// (for GTX1060)
// nvcc -arch=compute_61 -code=sm_61,sm_61 -O2 -m64 -o vecAdd vecAdd.cu


// Includes
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

enum {
    ARGUMENT_ERR = 1,
    DEVICE_ERR,
    THREADS_ERR
};

int lowbit(int x) { return x & (-x); }
void RandomInit(float[], int);

__global__ void VecMax(const float arr[], float res[], int N) {
    extern __shared__ float cache[];
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int cid = threadIdx.x;
    
    if (idx < N) {
        cache[cid] = arr[idx];
    }
    for (int step = blockDim.x >> 1; step; step >>= 1) {
        __syncthreads();
        if (cid < step) {
            cache[cid] = max(cache[cid], cache[cid + step]);
        }
    }
    if (cid == 0) {
        res[blockIdx.x] = cache[cid];
    }
}

int main(int argc, char* argv[]) {
    // parse arguments
    if (argc < 3) {
        printf("Usage: %s threadsPerBlock blocksPerGrid <deviceID> <N> <iter>\n", argv[0]);
        exit(ARGUMENT_ERR);
    }
    bool benchmark = false;
    int threadsPerBlock, blocksPerGrid, deviceID = 0, N = 81920007, iter = 1;
    threadsPerBlock = atoi(argv[1]);
    blocksPerGrid = atoi(argv[2]);
    if (argc >= 4) {
        deviceID = atoi(argv[3]);
    }
    if (argc >= 5) {
        N = atoi(argv[4]);
    }
    if (argc >= 6) {
        iter = atoi(argv[5]);
        benchmark = true;
    }

    if (cudaSetDevice(deviceID) != cudaSuccess) {
        fprintf(stderr, "!!! Cannot select GPU with device ID = %d\n", deviceID);
        exit(DEVICE_ERR);
    }
    if (threadsPerBlock != lowbit(threadsPerBlock)) {
        fprintf(stderr, "The number of threads per block must be power of 2\n");
        exit(THREADS_ERR);
    }
    if (threadsPerBlock > 1024) {
        fprintf(stderr, "The number of threads per block must be less than 1024 !\n");
        exit(THREADS_ERR);
    }
    if (threadsPerBlock * blocksPerGrid < N) {
        fprintf(stderr, "Not eoungh threads.\n");
        exit(THREADS_ERR);
    }


    const long size = N * sizeof(float);
    const long bks = blocksPerGrid * sizeof(float);
    const long tds = threadsPerBlock * sizeof(float);

    // initialize value for benchmark
    float Isum = 0, Issum = 0;
    float Gsum = 0, Gssum = 0;
    float Osum = 0, Ossum = 0;
    float Tsum = 0, Tssum = 0;
    float Csum = 0, Cssum = 0;
    float Esum = 0, Essum = 0;

    // prepare memory
    float *hostA = (float*)malloc(size);
    float *hostB = (float*)malloc(bks);
    float *deviceA, *deviceB;
    cudaMalloc((void**)&deviceA, size);
    cudaMalloc((void**)&deviceB, bks);
    // prepare timer
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    // start calculating
    for (int i = 0; i < iter; ++i) {
        RandomInit(hostA, N);
        
        /* copy data to GPU */
        // start the timer
        cudaEventRecord(start,0);

        // Copy vectors from host memory to device memory
        cudaMemcpy(deviceA, hostA, size, cudaMemcpyHostToDevice);

        // stop the timer
        cudaEventRecord(stop,0);
        cudaEventSynchronize(stop);

        float Intime;
        cudaEventElapsedTime(&Intime, start, stop);
        Isum += Intime;
        Issum += Intime * Intime;


        /* run kernel code */
        // start the timer
        cudaEventRecord(start, 0);

        VecMax<<<blocksPerGrid, threadsPerBlock, tds>>>(deviceA, deviceB, N);

        // stop the timer
        cudaEventRecord(stop, 0);
        cudaEventSynchronize(stop);

        float gputime;
        cudaEventElapsedTime(&gputime, start, stop);
        Gsum += gputime;
        Gssum += gputime * gputime;

        /* copy data from GPU */
        // start the timer
        cudaEventRecord(start,0);
        cudaMemcpy(hostB, deviceB, bks, cudaMemcpyDeviceToHost);
        float maxElementGPU = -1.0; // min element of RandomInit
        for(int j = 0; j < blocksPerGrid; ++j) 
            maxElementGPU = max(maxElementGPU, hostB[j]);

        // stop the timer
        cudaEventRecord(stop,0);
        cudaEventSynchronize(stop);

        float Outime;
        cudaEventElapsedTime(&Outime, start, stop);
        Osum += Outime;
        Ossum += Outime * Outime;

        float gputime_tot;
        gputime_tot = Intime + gputime + Outime;
        Tsum += gputime_tot;
        Tssum += gputime_tot * gputime_tot;


        /* CPU method */
        // start the timer
        cudaEventRecord(start, 0);

        // to compute the reference solution
        float maxElementCPU = -1.0; // min elemnt of RandomInit
        for(int j = 0; j < N; j++) 
            maxElementCPU = max(maxElementCPU, hostA[j]);

        // stop the timer
        cudaEventRecord(stop, 0);
        cudaEventSynchronize(stop);

        float cputime;
        cudaEventElapsedTime(&cputime, start, stop);
        Csum += cputime;
        Cssum += cputime * cputime;

        Esum += fabs(maxElementGPU - maxElementCPU);
        Essum += (maxElementGPU - maxElementCPU) * (maxElementGPU - maxElementCPU);
    }
    free(hostA);
    free(hostB);
    cudaFree(deviceA);
    cudaFree(deviceB);

    if (benchmark) {
        printf("%d iterations.\n", iter);
        
        Esum /= iter;
        Essum /= iter;
        printf("error: %f (avg), %f (SD)\n", Esum, sqrt(Essum - Esum * Esum));

        Isum /= iter;
        Issum /= iter;
        printf("input time: %fms (avg), %fms (SD)\n", Isum, sqrt(Issum - Isum * Isum));

        Gsum /= iter;
        Gssum /= iter;
        printf("gpu time: %fms (avg), %fms (SD)\n", Gsum, sqrt(Gssum - Gsum * Gsum));

        Osum /= iter;
        Ossum /= iter;
        printf("output time: %fms (avg), %fms (SD)\n", Osum, sqrt(Ossum - Osum * Osum));

        Tsum /= iter;
        Tssum /= iter;
        printf("total time: %fms (avg), %fms (SD)\n", Tsum, sqrt(Tssum - Tsum * Tsum));

        Csum /= iter;
        Cssum /= iter;
        printf("Processing time for CPU: %fms (avg), %fms (SD) \n", Csum, sqrt(Cssum - Csum * Csum));
        printf("Average Speed up of GPU = %f\n", Csum / Tsum);
    } else {
        printf("Input time for GPU: %f (ms) \n", Isum);
        printf("Processing time for GPU: %f (ms) \n", Gsum);
        printf("Output time for GPU: %f (ms) \n", Osum);
        printf("Total time for GPU: %f (ms) \n", Tsum);
        printf("Processing time for CPU: %f (ms) \n", Csum);
        printf("Speed up of GPU = %f\n", Csum / Tsum);

        printf("Check result:\n");
        printf("Average differences: %f\n", Esum);
    }
    return 0;
}

void RandomInit(float arr[], int n) {
    for (int i = 0; i < n; ++i)
        arr[i] = (2.0 * rand() / RAND_MAX) - 1;
}

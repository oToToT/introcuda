for ((i=1; i<=32; i=i+i))
do
    echo $i 2
    (
        echo 1 2
        echo 0 1
        echo 1024 1024
        echo $i $i
    ) > results/Input_${i}_2_1
    ./heatDiffusion < results/Input_${i}_2_1 > results/Output_${i}_2_1
    echo $i 2
    (
        echo 2 1
        echo 0 1
        echo 1024 1024
        echo $i $i
    ) > results/Input_${i}_2_2
    ./heatDiffusion < results/Input_${i}_2_2 > results/Output_${i}_2_2
    echo $i 1
    (
        echo 1 1
        echo 0
        echo 1024 1024
        echo $i $i
    ) > results/Input_${i}_1
    ./heatDiffusion < results/Input_${i}_1 > results/Output_${i}_1
done

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <omp.h>

const float eps = 1e-6;
const int ITER_MAX = 1000000;

// dOs[0], dU, dD, dL, dR, dOs[1], deviceResult[cpu_thread_id]
__global__ void solveGPU(float *dO, float *dU, float *dD, float *dL,
        float *dR, float *dN, float *res) {
    extern __shared__ float cache[];
    const int Lx = blockDim.x * gridDim.x;
    const int Ly = blockDim.y * gridDim.y;
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    const int j = blockIdx.y * blockDim.y + threadIdx.y;
    const int cacheId = threadIdx.x * blockDim.y + threadIdx.y;
    const int blockId = blockIdx.x * gridDim.y + blockIdx.y;
    const int bN = blockDim.x * blockDim.y;

    bool side = false;
    float L = 0, R = 0, U = 0, D = 0;

    if (i != 0) {
        U = dO[(i - 1) * Ly + j];
    } else if (dU){
        U = dU[(Lx - 1) * Ly + j];
    } else {
        side = true;
    }
    if (i != Lx - 1) {
        D = dO[(i + 1) * Ly + j];
    } else if (dD) {
        D = dD[0 * Ly + j];
    } else {
        side = true;
    }
    if (j != 0) {
        L = dO[i * Ly + (j - 1)];
    } else if (dL) {
        L = dL[i * Ly + (Ly - 1)];
    } else {
        side = true;
    }
    if (j != Ly - 1) {
        R = dO[i * Ly + (j + 1)];
    } else if (dR) {
        R = dR[i * Ly + 0];
    } else {
        side = true;
    }

    float diff = 0;
    if (!side) {
        dN[i * Ly + j] = (L + R + U + D) / 4;
        diff = dN[i * Ly + j] - dO[i * Ly + j];
    }

    cache[cacheId] = diff * diff;
    for (int step = bN >> 1; step; step >>= 1) {
        __syncthreads();
        if (cacheId < step) {
            cache[cacheId] += cache[cacheId + step];
        }
    }
    if (cacheId == 0) res[blockId] = cache[0];
}

void getNumOfGpu(int *a, int *b) {
    puts("Enter the number of GPUs (NGx, NGy): ");
    scanf("%d%d", a, b);
    printf("%d %d\n", *a, *b);
}

void getDevices(int n, int a[]) {
    puts("Enter GPU IDs (0/1/...): ");
    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
        printf("%d%c", a[i], " \n"[i == n - 1]);
    }
}

void getGridSize(int *a, int *b) {
    puts("Enter the size (Nx, Ny) of the 2D grid: ");
    scanf("%d%d", a, b);
    printf("%d %d\n", *a, *b);
}

void getBlockSize(int *a, int *b) {
    puts("Enter the number of threads (tx,ty) per block:");
    scanf("%d%d", a, b);
    printf("%d %d\n", *a, *b);
    if ((*a) * (*b) > 1024) {
        puts("!!! The number of threads per block must be less than 1024.");
        exit(0);
    }
}

void initializePlate(float *h, int n, int m) {
    for (int i = 0; i < n; ++i) {
        h[i * m + 0] = 273;
        h[i * m + (m - 1)] = 273;
    }
    for (int j = 0; j < m; ++j) {
        h[0 * m + j] = 400;
        h[(n - 1) * m + j] = 273;
    }
}

void savePlate(const char *fname, float *f, int n, int m) {
    // Save initial configuration in phi_initial.dat 
    FILE *out = fopen(fname, "w");
    if (out == NULL) {
        printf("!!! Cannot open file: %s\n", fname);
        exit(1);
    }
    fprintf(out, "%d %d\n", n, m);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            fprintf(out, "%.2e%c", f[i * m + j], " \n"[j == m - 1]);
        }
    }
    fclose(out);
}

//initializeGpu(Devices, device, host, deviceResult,
//            NGx, NGy, sizeN, NGPU, sizeBlock, Lx, Ly, Nx, Ny);
void initializeGpu(int Devices[], 
        float **device[], float *host[],
        float *deviceResult[], int NGx, int NGy,
        int sizeN, int NGPU, int sizeBlock,
        int Lx, int Ly, int Nx, int Ny) {

    cudaSetDevice(Devices[0]);
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    #pragma omp parallel num_threads(NGPU)
    {
        int cpu_thread_id = omp_get_thread_num();
        cudaSetDevice(Devices[cpu_thread_id]);
        int cpuid_x = cpu_thread_id / NGy;
        int cpuid_y = cpu_thread_id % NGy;

        if (cpuid_x != 0) {
            int cpuid_u = (cpuid_x - 1) * NGy + cpuid_y;
            cudaDeviceEnablePeerAccess(Devices[cpuid_u], 0);
        }
        if (cpuid_x != NGx - 1) {
            int cpuid_d = (cpuid_x + 1) * NGy + cpuid_y;
            cudaDeviceEnablePeerAccess(Devices[cpuid_d], 0);
        }
        if (cpuid_y != 0) {
            int cpuid_l = cpuid_x * NGy + (cpuid_y - 1);
            cudaDeviceEnablePeerAccess(Devices[cpuid_l], 0);
        }
        if (cpuid_y != NGy - 1) {
            int cpuid_r = cpuid_x * NGy + (cpuid_y + 1);
            cudaDeviceEnablePeerAccess(Devices[cpuid_r], 0);
        }

        #pragma omp critical
        {
            cudaMalloc((void**)&device[0][cpu_thread_id], sizeN / NGPU);
            cudaMalloc((void**)&device[1][cpu_thread_id], sizeN / NGPU);
            cudaMalloc((void**)&deviceResult[cpu_thread_id], sizeBlock / NGPU);
        }

        for (int t = 0; t < 2; ++t) {
            for (int i = 0; i < Lx; ++i) {
                float *h = host[t] + (cpuid_x * Lx + i) * Ny + cpuid_y * Ly;
                float *d = device[t][cpu_thread_id] + i * Ly;
                cudaMemcpy(d, h, Ly * sizeof(float), cudaMemcpyHostToDevice);
            }
        }
    }

    cudaSetDevice(Devices[0]);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    float Intime;
    cudaEventElapsedTime(&Intime, start, stop);
    printf("Data input time for GPU: %f (ms) \n", Intime);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);
}

int solve(int Devices[], float **device[],
        float *deviceResult[], float hostResult[],
        int NGx, int NGy, int sizeBlock, int NGPU, int Bx,
        int By, size_t shareMem, dim3 blocks, dim3 threads) {
    
    cudaSetDevice(Devices[0]);
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
   
    cudaEventRecord(start, 0);

    float error = eps + 1;
    int iter;
    for (iter = 0; iter < ITER_MAX && error > eps; ++iter) {
        bool flag = iter & 1;
        #pragma omp parallel num_threads(NGPU)
        {
            int cpu_thread_id = omp_get_thread_num();
            cudaSetDevice(Devices[cpu_thread_id]);
            int cpuid_x = cpu_thread_id / NGy;
            int cpuid_y = cpu_thread_id % NGy;

            float **d[] = {device[flag ^ 1], device[flag]};
            float *dOs[] = {d[0][cpu_thread_id], d[1][cpu_thread_id]};

            float *dU = NULL, *dD = NULL, *dL = NULL, *dR = NULL;
            if (cpuid_x != 0) {
                int cpuid_u = (cpuid_x - 1) * NGy + cpuid_y;
                dU = d[0][cpuid_u];
            }
            if (cpuid_x != NGx - 1) {
                int cpuid_d = (cpuid_x + 1) % NGy * NGy + cpuid_y;
                dD = d[0][cpuid_d];
            }
            if (cpuid_y != 0) {
                int cpuid_l = cpuid_x * NGy + (cpuid_y - 1 + NGy) % NGy;
                dL = d[0][cpuid_l];
            }
            if (cpuid_y != NGy - 1) {
                int cpuid_r = cpuid_x * NGy + (cpuid_y + 1) % NGy;
                dR = d[0][cpuid_r];
            }

            solveGPU<<<blocks, threads, shareMem>>>(
                dOs[0], dU, dD, dL, dR,
                dOs[1], deviceResult[cpu_thread_id]
            );
            cudaMemcpy(
                hostResult + Bx * By / NGPU * cpu_thread_id,
                deviceResult[cpu_thread_id],
                sizeBlock / NGPU, cudaMemcpyDeviceToHost
            );
        }
        error = 0;
        for (int i = 0; i < Bx * By; ++i) {
            error += hostResult[i];
        }
        error = sqrt(error);
    }
    
    cudaSetDevice(Devices[0]);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    float GPUtime;
    cudaEventElapsedTime(&GPUtime, start, stop);
    printf("Processing time for GPU: %f (ms) \n", GPUtime);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    return iter;
}

void copyResult(int Devices[], float *device[], float *result,
        int Lx, int Ly, int NGx, int NGy, int Nx, int Ny, int NGPU) {
   
    cudaSetDevice(Devices[0]);
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    #pragma omp parallel num_threads(NGPU)
    {
        int cpu_thread_id = omp_get_thread_num();
        cudaSetDevice(Devices[cpu_thread_id]);

        int cpuid_x = cpu_thread_id / NGy;
        int cpuid_y = cpu_thread_id % NGy;
        for (int i = 0; i < Lx; ++i) {
	        float *g = result + (cpuid_x * Lx + i) * Ny + cpuid_y * Ly;
	        float *d = device[cpu_thread_id] + i * Ly;
            cudaMemcpy(g, d, Ly * sizeof(float), cudaMemcpyDeviceToHost);
        }
    } 
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    float outime;
    cudaEventElapsedTime(&outime, start, stop);
    printf("Data output time for GPU: %f (ms) \n", outime);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);
}

int main() {
    int NGx, NGy;
    getNumOfGpu(&NGx, &NGy);
    const int NGPU = NGx * NGy;
    int *Devices = (int *)malloc(NGPU * sizeof(int));
    getDevices(NGPU, Devices);
    int Nx, Ny;
    getGridSize(&Nx, &Ny);
    int tx, ty;
    getBlockSize(&tx, &ty);
    if (Nx % tx != 0 ||
            Nx / tx % NGx != 0 ||
            Ny % ty != 0 ||
            Ny / ty % NGy != 0) {
        puts("Invalid Configuration");
        exit(1);
    }

    int Bx = Nx / tx, By = Ny / ty;
    int Lx = Nx / NGx, Ly = Ny / NGy;
    dim3 threads(tx, ty);
    dim3 blocks(Bx / NGx, By / NGy);
    int N = Nx * Ny;
    size_t sizeN = N * sizeof(float);
    size_t sizeBlock = Bx * By * sizeof(float);
    size_t shareMem = tx * ty * sizeof(float);

    float *host[2] = {
        (float *)malloc(sizeN),
        (float *)malloc(sizeN)
    };
    float *hostResult = (float *)malloc(sizeBlock);
    float *result = (float *)malloc(sizeN);

    initializePlate(host[0], Nx, Ny);
    initializePlate(host[1], Nx, Ny);

    savePlate("phi.dat", host[0], Nx, Ny);

    float **device[] = {
        (float **)malloc(NGPU * sizeof(float *)),
        (float **)malloc(NGPU * sizeof(float *))
    };
    float **deviceResult = (float **)malloc(NGPU * sizeof(float *));

    initializeGpu(Devices, device, host, deviceResult,
            NGx, NGy, sizeN, NGPU, sizeBlock, Lx, Ly, Nx, Ny);
    int iter = solve(Devices, device, deviceResult, hostResult,
            NGx, NGy, sizeBlock, NGPU, Bx, By,
            shareMem, blocks, threads);
    copyResult(Devices, device[iter & 1], result, Lx, Ly, NGx, NGy, Nx, Ny, NGPU);
    savePlate("phi_GPU.dat", result, Nx, Ny);
   
    free(host[0]);
    free(host[1]);
    free(hostResult);
    free(result);
    for (int i = 0; i < NGPU; ++i) {
        cudaSetDevice(Devices[i]);
        cudaFree(device[0][i]);
        cudaFree(device[1][i]);
        cudaFree(deviceResult[i]);
    }
    free(device[0]);
    free(device[1]);
    free(deviceResult);

    #pragma omp parallel num_threads(NGPU)
    {
        int cpu_thread_id = omp_get_thread_num();
        cudaSetDevice(Devices[cpu_thread_id]);
        cudaDeviceReset();
    }
    free(Devices);
    return 0;
}

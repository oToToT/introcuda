#include <cstdio>
#include <chrono>
#include <complex>
#include <cinttypes>
#include <cufft.h>
using llf = double;
using C = std::complex<llf>;

const C I = C(0, 1);
const llf PI = 3.1415926535897932384626433832795028841971693993751058;
const llf EPS = 1e-5;

int main() {
    puts("lattice size:");
    int n; scanf("%d", &n);
    printf("%d\n", n);

    llf h = static_cast<llf>(1) / (n - 1);
    llf q = 10;

    C *rho = (C *)malloc(sizeof(C) * n * n * n);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
                if (i == n / 2 && j == n / 2 && k == n / 2)
                    rho[i * n * n + j * n + k] = q / (h * h * h);
                else rho[i * n * n + j * n + k] = 0;
            }
        }
    }


    C *f_ = (C *)malloc(sizeof(C) * n);
    cufftHandle plan;
    cufftDoubleComplex *f = nullptr;
    cudaMalloc((void**)&f, sizeof(C) * n);

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    // 3D FFT
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
                f_[k] = rho[i * n * n + j * n + k];
            }
            cudaMemcpy(f, f_, sizeof(C) * n , cudaMemcpyHostToDevice);
            cufftPlan1d(&plan, n, CUFFT_Z2Z, 1);
            cufftExecZ2Z(plan, f, f, CUFFT_FORWARD);
            cudaMemcpy(f_, f, sizeof(C) * n, cudaMemcpyDeviceToHost);
            for (int k = 0; k < n; ++k) {
                rho[i * n * n + j * n + k] = f_[k];
            }
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int k = 0; k < n; ++k) {
            for (int j = 0; j < n; ++j) {
                f_[j] = rho[i * n * n + j * n + k];
            }
            cudaMemcpy(f, f_, sizeof(C) * n , cudaMemcpyHostToDevice);
            cufftPlan1d(&plan, n, CUFFT_Z2Z, 1);
            cufftExecZ2Z(plan, f, f, CUFFT_FORWARD);
            cudaMemcpy(f_, f, sizeof(C) * n, cudaMemcpyDeviceToHost);
            for (int j = 0; j < n; ++j) {
                rho[i * n * n + j * n + k] = f_[j];
            }
        }
    }
    for (int k = 0; k < n; ++k) {
        for (int j = 0; j < n; ++j) {
            for (int i = 0; i < n; ++i) {
                f_[i] = rho[i * n * n + j * n + k];
            }
            cudaMemcpy(f, f_, sizeof(C)* n , cudaMemcpyHostToDevice);
            cufftPlan1d(&plan, n, CUFFT_Z2Z, 1);
            cufftExecZ2Z(plan, f, f, CUFFT_FORWARD);
            cudaMemcpy(f_, f, sizeof(C) * n, cudaMemcpyDeviceToHost);
            for (int i = 0; i < n; ++i) {
                rho[i * n * n + j * n + k] = f_[i];
            }
        }
    }

    // solve  laplace
    C W = exp(2 * PI * I / static_cast<llf>(n));
    C Wx = 1, Wy = 1, Wz = 1;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
                C d = 4;
                d -= (Wx + 1. / Wx) + (Wy + 1. / Wy) + (Wz + 1. / Wz);
                if (abs(d) > EPS) rho[i * n * n + j * n + k] *= h * h * h / d;
                Wz *= W;
            }
            Wy *= W;
        }
        Wz *= W;
    }

    // 3D iFFT
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
                f_[k] = rho[i * n * n + j * n + k];
            }
            cudaMemcpy(f, f_, sizeof(C)* n , cudaMemcpyHostToDevice);
            cufftPlan1d(&plan, n, CUFFT_Z2Z, 1);
            cufftExecZ2Z(plan, f, f, CUFFT_INVERSE);
            cudaMemcpy(f_, f, sizeof(C) * n, cudaMemcpyDeviceToHost);
            for (int k = 0; k < n; ++k) {
                rho[i * n * n + j * n + k] = f_[k];
            }
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int k = 0; k < n; ++k) {
            for (int j = 0; j < n; ++j) {
                f_[j] = rho[i * n * n + j * n + k];
            }
            cudaMemcpy(f, f_, sizeof(C)* n , cudaMemcpyHostToDevice);
            cufftPlan1d(&plan, n, CUFFT_Z2Z, 1);
            cufftExecZ2Z(plan, f, f, CUFFT_INVERSE);
            cudaMemcpy(f_, f, sizeof(C) * n, cudaMemcpyDeviceToHost);
            for (int j = 0; j < n; ++j) {
                rho[i * n * n + j * n + k] = f_[j];
            }
        }
    }
    for (int k = 0; k < n; ++k) {
        for (int j = 0; j < n; ++j) {
            for (int i = 0; i < n; ++i) {
                f_[i] = rho[i * n * n + j * n + k];
            }
            cudaMemcpy(f, f_, sizeof(C)* n , cudaMemcpyHostToDevice);
            cufftPlan1d(&plan, n, CUFFT_Z2Z, 1);
            cufftExecZ2Z(plan, f, f, CUFFT_INVERSE);
            cudaMemcpy(f_, f, sizeof(C) * n, cudaMemcpyDeviceToHost);
            for (int i = 0; i < n; ++i) {
                rho[i * n * n + j * n + k] = f_[i];
            }
        }
    }

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    uint64_t t = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    printf("Processing time: %lfms \n", static_cast<llf>(t) / 1000);


    FILE *fp = fopen("poisson_fft.dat", "w");
    llf tot = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j)
            tot += real(rho[i * n * n + j * n + j]);
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
                fprintf(fp, "%.5e%c", real(rho[i * n * n + j * n + k]), " \n"[k == n - 1]);
            }
            fputs("-", fp);
        }
    }
    printf("result: %.5e\n", tot);
    fclose(fp);
    free(rho);
    free(f_);
    cufftDestroy(plan);
    cudaFree(f);
    return 0;
}

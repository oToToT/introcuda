#!/bin/bash

for ((i=20;i<=500;i=i+24))
do
    echo $i
    echo $i | ./poisson_fft_3d > results/3d_$i
    echo $i | ./poisson_fft > results/1d_$i
done

// Matrix addition: C = A + B.
// compile with the following command:
//
// (for GTX970)
// nvcc -arch=compute_52 -code=sm_52,sm_52 -O2 -m64 -o vecAdd vecAdd.cu
//
// (for GTX1060)
// nvcc -arch=compute_61 -code=sm_61,sm_61 -O2 -m64 -o vecAdd vecAdd.cu


// Includes
#include <stdio.h>
#include <stdlib.h>

enum {
    DEVICE_ERR = 1,
    THREADS_ERR
};


// Device Code
__global__ void MatAdd(const float *A, const float *B, float *C, int n) {

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int idx = j * n + i;

    if (i < n && j < n) {
        C[idx] = A[idx] + B[idx];
    }

    __syncthreads();
}


void RandomInit(float*, int);

int main(int argc, char* argv[]) {
    if (argc < 2) {
        printf("Usage:\n%s blockX blockY <deviceID> <N>\n", argv[0]);
    }

    int blockX, blockY, deviceID = 0, N = 6400;
    blockX = atoi(argv[1]);
    blockY = atoi(argv[2]);

    if (argc >= 4) {
        deviceID = atoi(argv[3]);
    }
    if (argc >= 5) {
        N = atoi(argv[4]);
    }

    if (cudaSetDevice(deviceID) != cudaSuccess) {
        fprintf(stderr, "!!! Cannot select GPU with device ID = %d\n", deviceID);
        exit(DEVICE_ERR);
    }

    if (blockX * blockY > 1024) {
        fputs("The number of threads per block must be less than 1024 !\n", stderr);
        exit(THREADS_ERR);
    }

    printf("Current Block Size: %d x %d\n", blockX, blockY);

    dim3 dimBlock(blockX, blockY);
    dim3 dimGrid((N + blockX - 1) / blockX, (N + blockY - 1) / blockY);

    const long size = N * N * sizeof(float);

    // allocate 3 N * N matrix
    float *hostA, *hostB, *hostC;
    hostA = (float *)malloc(size);
    hostB = (float *)malloc(size);
    hostC = (float *)malloc(size);

    // initialize matrix with random value
    RandomInit(hostA, N);
    RandomInit(hostB, N);


    float *deviceA, *deviceB, *deviceC;
    // crate cuda timer
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // start the timer
    cudaEventRecord(start, 0);

    // Allocate vectors in device memory
    cudaMalloc((void**)&deviceA, size);
    cudaMalloc((void**)&deviceB, size);
    cudaMalloc((void**)&deviceC, size);

    // Copy vectors from host memory to device memory

    cudaMemcpy(deviceA, hostA, size, cudaMemcpyHostToDevice);
    cudaMemcpy(deviceB, hostB, size, cudaMemcpyHostToDevice);

    // stop the timer
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float Intime;
    cudaEventElapsedTime(&Intime, start, stop);
    printf("Input time for GPU: %f (ms) \n", Intime);

    // start the timer
    cudaEventRecord(start, 0);

    MatAdd<<<dimGrid, dimBlock>>>(deviceA, deviceB, deviceC, N);

    // stop the timer
    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);

    float gputime;
    cudaEventElapsedTime(&gputime, start, stop);
    printf("Processing time for GPU: %f (ms) \n", gputime);
    printf("GPU Gflops: %f\n", N / (1000000.0 * gputime));

    // Copy result from device memory to host memory
    // hostC contains the result in host memory

    // start the timer
    cudaEventRecord(start,0);

    cudaMemcpy(hostC, deviceC, size, cudaMemcpyDeviceToHost);
    cudaFree(deviceA);
    cudaFree(deviceB);
    cudaFree(deviceC);

    // stop the timer
    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);

    float Outime;
    cudaEventElapsedTime(&Outime, start, stop);
    printf("Output time for GPU: %f (ms) \n", Outime);

    float gputime_tot;
    gputime_tot = Intime + gputime + Outime;
    printf("Total time for GPU: %f (ms) \n\n", gputime_tot);

    // start the timer
    cudaEventRecord(start, 0);

    float *hostD;
    hostD = (float*)malloc(size);       // to compute the reference solution
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            // reorder the calculation order to improve performance
            hostD[i * N + j] = hostA[i * N + j] + hostB[i * N + j];
        }
    }

    // stop the timer
    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);

    float cputime;
    cudaEventElapsedTime(&cputime, start, stop);
    printf("Processing time for CPU: %f (ms) \n", cputime);
    printf("CPU Gflops: %f\n", N / (1000000.0 * cputime));
    printf("Speed up of GPU = %f\n", cputime / (gputime_tot));

    // destroy the timer
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    // check result

    puts("Check result:");
    double sum = 0; 
    double diff;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            diff = abs(hostD[j * N + i] - hostC[j * N + i]);
            sum += diff * diff;
        }
    }
    sum = sqrt(sum);
    printf("norm(hostC - hostD)=%20.15e\n",sum);

    cudaDeviceReset();
    
    return 0;
}

void RandomInit(float* mat, int n) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++ j) {
            mat[j * n + i] = rand() / (float)RAND_MAX;
        }
    }
}

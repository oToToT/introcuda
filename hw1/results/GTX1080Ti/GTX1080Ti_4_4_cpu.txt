Current Block Size: 4 x 4
Input time for GPU: 43.489441 (ms) 
Processing time for GPU: 5.111104 (ms) 
GPU Gflops: 0.001252
Output time for GPU: 114.871681 (ms) 
Total time for GPU: 163.472229 (ms) 

Processing time for CPU: 99.396225 (ms) 
CPU Gflops: 0.000064
Speed up of GPU = 0.608031
Check result:
norm(hostC - hostD)=0.000000000000000e+00

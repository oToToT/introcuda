Current Block Size: 20 x 20
Input time for GPU: 41.690239 (ms) 
Processing time for GPU: 3.147840 (ms) 
GPU Gflops: 0.002033
Output time for GPU: 115.318687 (ms) 
Total time for GPU: 160.156769 (ms) 

Processing time for CPU: 98.948929 (ms) 
CPU Gflops: 0.000065
Speed up of GPU = 0.617825
Check result:
norm(hostC - hostD)=0.000000000000000e+00

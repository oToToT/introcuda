Current Block Size: 16 x 16

Input time for GPU: 82.296257 (ms) 
Processing time for GPU: 1.894464 (ms) 
GPU Gflops: 0.003378
Output time for GPU: 140.884415 (ms) 
Total time for GPU: 225.075134 (ms) 

Processing time for CPU: 1725.432495 (ms) 
CPU Gflops: 0.000004
Speed up of GPU = 7.666029
Check result:
norm(hostC - hostD)=0.000000000000000e+00

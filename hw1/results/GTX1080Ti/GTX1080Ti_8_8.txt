Current Block Size: 8 x 8

Input time for GPU: 49.843231 (ms) 
Processing time for GPU: 2.638400 (ms) 
GPU Gflops: 0.002426
Output time for GPU: 114.174080 (ms) 
Total time for GPU: 166.655716 (ms) 

Processing time for CPU: 1305.730469 (ms) 
CPU Gflops: 0.000005
Speed up of GPU = 7.834898
Check result:
norm(hostC - hostD)=0.000000000000000e+00

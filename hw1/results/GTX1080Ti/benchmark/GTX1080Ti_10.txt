Current Block Size: 10 x 10
50 iteration
input time: 41.034622ms (avg), 1.076652ms (SD)
gpu time: 1.659323ms (avg), 0.002970ms (SD)
output time: 29.163015ms (avg), 11.015276ms (SD)
total time: 71.856972ms (avg), 10.984853ms (SD)
Processing time for CPU: 1025.049438ms (avg), 38.844242ms (SD) 
CPU Gflops: 0.000006
Speed up of GPU = 14.265136

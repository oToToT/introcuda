Current Block Size: 32 x 32
Input time for GPU: 51.950047 (ms) 
Processing time for GPU: 0.897888 (ms) 
GPU Gflops: 0.007128
Output time for GPU: 100.526718 (ms) 
Total time for GPU: 153.374649 (ms) 

Processing time for CPU: 98.765152 (ms) 
CPU Gflops: 0.000065
Speed up of GPU = 0.643947
Check result:
norm(hostC - hostD)=0.000000000000000e+00

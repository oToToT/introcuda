Current Block Size: 8 x 8

Input time for GPU: 42.052032 (ms) 
Processing time for GPU: 0.891712 (ms) 
GPU Gflops: 0.007177
Output time for GPU: 101.351616 (ms) 
Total time for GPU: 144.295364 (ms) 

Processing time for CPU: 1056.347412 (ms) 
CPU Gflops: 0.000006
Speed up of GPU = 7.320730
Check result:
norm(hostC - hostD)=0.000000000000000e+00

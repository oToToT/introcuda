Current Block Size: 20 x 20

Input time for GPU: 40.796352 (ms) 
Processing time for GPU: 0.932928 (ms) 
GPU Gflops: 0.006860
Output time for GPU: 100.855682 (ms) 
Total time for GPU: 142.584961 (ms) 

Processing time for CPU: 1035.078613 (ms) 
CPU Gflops: 0.000006
Speed up of GPU = 7.259381
Check result:
norm(hostC - hostD)=0.000000000000000e+00

Current Block Size: 8 x 8
Input time for GPU: 44.757153 (ms) 
Processing time for GPU: 0.992224 (ms) 
GPU Gflops: 0.006450
Output time for GPU: 135.307098 (ms) 
Total time for GPU: 181.056473 (ms) 

Processing time for CPU: 100.674690 (ms) 
CPU Gflops: 0.000064
Speed up of GPU = 0.556040
Check result:
norm(hostC - hostD)=0.000000000000000e+00

Current Block Size: 10 x 10
50 iteration
input time: 3071.057129ms (avg), 548.518005ms (SD)
gpu time: 1.906335ms (avg), 0.670700ms (SD)
output time: 1288.653931ms (avg), 2001.673462ms (SD)
total time: 4361.617676ms (avg), 1949.960571ms (SD)
Processing time for CPU: 90.687050ms (avg), 5.545180ms (SD) 
CPU Gflops: 0.000071
Speed up of GPU = 0.020792

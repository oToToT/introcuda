Current Block Size: 20 x 20
50 iteration
input time: 3028.568848ms (avg), 548.246277ms (SD)
gpu time: 1.922256ms (avg), 0.638726ms (SD)
output time: 1448.984009ms (avg), 2206.725098ms (SD)
total time: 4479.475098ms (avg), 2139.823730ms (SD)
Processing time for CPU: 91.651054ms (avg), 5.468661ms (SD) 
CPU Gflops: 0.000070
Speed up of GPU = 0.020460

Current Block Size: 16 x 16
50 iteration
input time: 3066.817871ms (avg), 555.308899ms (SD)
gpu time: 1.894355ms (avg), 0.629167ms (SD)
output time: 1208.770630ms (avg), 1803.033325ms (SD)
total time: 4277.482910ms (avg), 1799.253784ms (SD)
Processing time for CPU: 92.104561ms (avg), 7.356571ms (SD) 
CPU Gflops: 0.000069
Speed up of GPU = 0.021532

Current Block Size: 8 x 8

Input time for GPU: 56.349663 (ms) 
Processing time for GPU: 3.712256 (ms) 
GPU Gflops: 0.001724
Output time for GPU: 60.100159 (ms) 
Total time for GPU: 120.162079 (ms) 

Processing time for CPU: 1202.964844 (ms) 
CPU Gflops: 0.000005
Speed up of GPU = 10.011186
Check result:
norm(hostC - hostD)=0.000000000000000e+00

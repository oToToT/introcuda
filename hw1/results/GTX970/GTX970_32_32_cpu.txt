Current Block Size: 32 x 32
Input time for GPU: 56.171009 (ms) 
Processing time for GPU: 3.351072 (ms) 
GPU Gflops: 0.001910
Output time for GPU: 60.186562 (ms) 
Total time for GPU: 119.708641 (ms) 

Processing time for CPU: 62.101280 (ms) 
CPU Gflops: 0.000103
Speed up of GPU = 0.518770
Check result:
norm(hostC - hostD)=0.000000000000000e+00

Current Block Size: 8 x 8
50 iteration
input time: 56.621410ms (avg), 0.091109ms (SD)
gpu time: 3.546280ms (avg), 0.047078ms (SD)
output time: 41.680408ms (avg), 2.730175ms (SD)
total time: 101.848076ms (avg), 2.763285ms (SD)
Processing time for CPU: 63.315849ms (avg), 0.180872ms (SD) 
CPU Gflops: 0.000101
Speed up of GPU = 0.621670

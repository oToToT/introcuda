Current Block Size: 4 x 4
50 iteration
input time: 56.607384ms (avg), 0.235932ms (SD)
gpu time: 5.567648ms (avg), 0.200821ms (SD)
output time: 41.679237ms (avg), 2.719334ms (SD)
total time: 103.854279ms (avg), 2.734866ms (SD)
Processing time for CPU: 1210.225586ms (avg), 16.564268ms (SD) 
CPU Gflops: 0.000005
Speed up of GPU = 11.653112

Current Block Size: 32 x 32
50 iteration
input time: 56.543602ms (avg), 0.046875ms (SD)
gpu time: 3.339911ms (avg), 0.003782ms (SD)
output time: 41.648682ms (avg), 2.796984ms (SD)
total time: 101.532188ms (avg), 2.774571ms (SD)
Processing time for CPU: 1207.554321ms (avg), 15.532225ms (SD) 
CPU Gflops: 0.000005
Speed up of GPU = 11.893315

Current Block Size: 10 x 10
50 iteration
input time: 56.698257ms (avg), 0.235932ms (SD)
gpu time: 3.498319ms (avg), 0.040712ms (SD)
output time: 41.786655ms (avg), 2.756451ms (SD)
total time: 101.983231ms (avg), 2.918991ms (SD)
Processing time for CPU: 63.259697ms (avg), 0.357331ms (SD) 
CPU Gflops: 0.000101
Speed up of GPU = 0.620295

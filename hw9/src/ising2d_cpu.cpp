#include <chrono>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <random>
#include <algorithm>

using llf = double;
const llf EPS = 1e-8;
const llf PI = acos(-1);

std::mt19937 rnd(7122);
std::uniform_real_distribution<llf> prob(0, 1);

void update_spin(int8_t *, int, llf, llf);
void exact_2d(llf, llf, llf *, llf *);

int main() {
    // input configuration
    puts("Enter the size of lattice");
    int n; scanf("%d", &n);
    printf("%d\n", n);

    puts("Enter the external magnetization");
    llf B; scanf("%lf", &B);
    printf("%lf\n", B);

    puts("Enter the temperature (in units of J/k)");
    llf T; scanf("%lf", &T);
    printf("%lf\n", T);

    puts("Enter the # of sweeps for thermalization");
    int nt; scanf("%d", &nt);
    printf("%d\n", nt);

    puts("Enter the # of measurements");
    int nm; scanf("%d", &nm);
    printf("%d\n", nm);

    puts("Enter the interval between successive measurements");
    int im; scanf("%d", &im);
    printf("%d\n", im);

    // initialize
    const int n2 = n * n;
    int8_t *spin = (int8_t *)malloc(n2 * sizeof(int8_t));
    memset(spin, 1, n2);

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    for (int i = 0; i < nt; ++i) {
        update_spin(spin, n, B, T);
    }


    // measurement
    for (int it0 = 0; it0 < nm; ++it0) {
        for (int it1 = 0; it1 < im; ++it1) {
            update_spin(spin, n, B, T);
        }
        llf mag = 0, ene = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                int ni = (i + 1) % n;
                int nj = (j + 1) % n;
                mag += spin[i * n + j];
                ene -= spin[i * n + j] * (spin[ni * n + j] + spin[i * n + nj]);
            }
        }
        ene -= B * mag;
        printf("%.5e\t%.5e\n", ene / n2, mag / n2);
    }
    
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    uint64_t t = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    printf("Processing time: %lfms \n", static_cast<llf>(t) / 1000);

    if(fabs(B) < EPS) {
        llf E_ex, M_ex;
        exact_2d(T, B, &E_ex, &M_ex);
        printf("E_exact=%.5e  M_exact=%.5e\n", E_ex, M_ex);
    }
    return 0;
}

void update_spin(int8_t *s, int n, llf B, llf T) {
    static const int dx[] = {0, 0, 1, -1};
    static const int dy[] = {1, -1, 0, 0};
    for (int e = 0; e < 2; ++e) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if ((i + j) % 2 != e)
                    continue;
                int spins = 0;
                for (int o = 0; o < 4; ++o) {
                    int ni = (i + dx[o] + n) % n;
                    int nj = (j + dy[o] + n) % n;
                    spins += s[ni * n + nj];
                }
                llf de = 2 * s[i * n + j] * (spins + B);
                if ((de <= 0) || (prob(rnd) < exp(-de / T))) {
                    s[i * n + j] = -s[i * n + j];
                }
            }
        }
    }
}

/********
 * ellf *   Elliptic integral of the 1st kind 
 ********/

llf rf(llf x, llf y, llf z) {
    static const llf ERRTOL = 0.08; 
    static const llf TINY = 1.5e-38; 
    static const llf BIG = 3.0e37; 
    static const llf THIRD = 1.0/3.0;
    static const llf C1 = 1.0/24.0; 
    static const llf C2 = 0.1; 
    static const llf C3 = 3.0/44.0; 
    static const llf C4 = 1.0/14.0;
    llf avg, delx, dely, delz;

    assert(std::min({x,y,z}) > 0);
    assert(std::min({x+y,x+z,y+z}) > TINY);
    assert(std::max({x,y,z}) < BIG);

    do {
        const llf sqrtx = sqrt(x);
        const llf sqrty = sqrt(y);
        const llf sqrtz = sqrt(z);
        const llf alamb = sqrtx * (sqrty + sqrtz) + sqrty * sqrtz;
        x = 0.25 * (x + alamb);
        y = 0.25 * (y + alamb);
        z = 0.25 * (z + alamb);
        avg = THIRD * (x + y + z);
        delx = (avg - x) / avg;
        dely = (avg - y) / avg;
        delz = (avg - z) / avg;
    } while (std::max({fabs(delx), fabs(dely), fabs(delz)}) > ERRTOL);

    const llf e2 = delx * dely - pow(delz, 2);
    const llf e3 = delx * dely * delz;

    return (1.0 + (C1 * e2 - C2 - C3 * e3) * e2 + C4 * e3) / sqrt(avg);
}

llf ellf(llf phi, llf ak) {
    const llf s=sin(phi);
    return s * rf(pow(cos(phi), 2), (1.0 - s * ak) * (1.0 + s * ak), 1.0);
}

void exact_2d(llf T, llf B, llf *E, llf *M) {
    assert(fabs(B) < EPS);

    llf x, y;
    llf z, Tc, K, K1;

    K = 2.0/T;
    Tc = -2.0/log(sqrt(2.0) - 1.0); // critical temperature;
    if(T > Tc) {
        *M = 0.0;
    } else if(T < Tc) {
        z = exp(-K);
        *M = pow(1.0 + z*z,0.25)*pow(1.0 - 6.0*z*z + pow(z,4),0.125)/sqrt(1.0 - z*z);
    }
    x = 0.5*PI;
    y = 2.0*sinh(K)/pow(cosh(K),2);
    K1 = ellf(x, y);
    *E = -1.0/tanh(K)*(1. + 2.0/PI*K1*(2.0*pow(tanh(K),2) - 1.0));

    return;
}

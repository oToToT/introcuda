#!/bin/bash

cat <<EOF > input
200
0
2
1000
10
1000
EOF

for ((i=1;i<=22;i=i+2))
do
    echo 1-$i
    ./ising2d_1gpu 0 $i $((i+i)) < input > results/gpu1_$i &
    pid=$!
    ./ising2d_1gpu 1 $((i+1)) $((i+i+2)) < input > results/gpu1_$((i+1))
    wait $pid
done

for ((i=1;i<=22;i=i+1))
do
    echo 2-$i
    ./ising2d_2gpu 0 1 $i $((i+i)) < input > results/gpu2_$i
done

for ((c=0;c<=5;c=c+1))
do
    echo $c
    cat <<EOF > input
200
0
2.$c
1000
10
1000
EOF
    ./ising2d_1gpu 1 4 8 < input > results/gpu1_4_$c
done

for ((c=0;c<=5;c=c+1))
do
    echo $c
    cat <<EOF > input
200
0
2.$c
1000
10
1000
EOF
    ./ising2d_2gpu 0 1 10 20 < input > results/gpu2_10_$c
done

for ((c=0;c<=5;c=c+1))
do
    echo $c
    cat <<EOF | ./ising2d_cpu < input > results/cpu_$c &
200
0
2.$c
1000
10
1000
EOF
done

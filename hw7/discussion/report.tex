\documentclass[a4paper,11pt]{article}
\usepackage[left=2cm, right=2cm, top=1.5cm, bottom=1.5cm]{geometry}
\usepackage{xeCJK}
\usepackage{indentfirst}
\usepackage{tabularx}
\usepackage{float}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsthm}
\newtheorem{lemma}{Lemma}
\newtheorem{claim}{Claim}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage[usenames,dvipsnames]{xcolor}

\setCJKmainfont{NotoSansCJKtc-Thin}
\setmonofont{Consolas}

\definecolor{CodeGreen}{rgb}{0,0.6,0}
\definecolor{CodeGray}{rgb}{0.5,0.5,0.5}
\definecolor{CodeMauve}{rgb}{0.58,0,0.82}
\lstset{
    basicstyle = \ttfamily\footnotesize, 
    breakatwhitespace = false,
    breaklines = true,         
    commentstyle = \color{CodeGreen}\bfseries,
    extendedchars = false,
    keepspaces=true,
    keywordstyle=\color{blue}\bfseries, % keyword style
    language = C++,                     % the language of code
    otherkeywords={string},
    numbers=left,
    numbersep=5pt,
    numberstyle=\tiny\color{CodeGray},
    rulecolor=\color{black},
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    stepnumber=1,       
    stringstyle=\color{CodeMauve},        % string literal style
    tabsize=2,
}
% from https://blog.csdn.net/RobertChenGuangzhi/article/details/45126785

\begin{document}
\pagestyle{fancy}
\fancyhead[L]{Introduction to CUDA Parallel Programming - Homework 7}
\fancyhead[R]{Author: b08902100 江昱勳}

\section{Monte Carlo integration in 10‐dimensions}

From figure \ref{fig:simple_cpu_val}, we can see that with larger sampling number, the error will be less and the value will become more close to actual value. This is very obvious because we might know more information after we sample more times.
Nonetheless, we should also notice that the time cost will increase simultaneously as figure \ref{fig:simple_cpu_time} shows.

\begin{figure}[h]
    \center
    \includegraphics[width=.85\textwidth]{simple_cpu_val.pdf}
    \caption{value with error}
    \label{fig:simple_cpu_val}
\end{figure}

\begin{figure}[h]
    \center
    \includegraphics[width=.85\textwidth]{simple_cpu_time.pdf}
    \caption{time cost by simple cpu}
    \label{fig:simple_cpu_time}
\end{figure}

To decrease the error of sampling, we can use \textbf{importance sampling} technique to sample with weight function.
This time, the weight function is $\Pi_{i=1}^{10} C e^{-ax_i}$. Though there seems to be two parameters $a$ and $C$, those parameters should fit into the constraint $\int W(X) = 1$, so the relation between $a$ and $C$ can be described as $\frac{C}{a} - \frac{C}{a} e^{-a} = 1$.
Notice that when $a=0$, \textbf{importance sampling} will degenerate to \textbf{simple sampling}.

After some experiment (see figure \ref{fig:import_cpu_64}, \ref{fig:import_cpu_128}, \ref{fig:import_cpu_256}, \ref{fig:import_cpu_512}, \ref{fig:import_cpu_1024}, \ref{fig:import_cpu_2048}, \ref{fig:import_cpu_4096}, \ref{fig:import_cpu_8192}, \ref{fig:import_cpu_16384}, \ref{fig:import_cpu_32768}, \ref{fig:import_cpu_65536}) with different $a$ values, I found that most of them have smallest error at $a=0.2$.
Also, we can see that the error might increase when $a$ is much more away from $0.2$, this might told us that the distribution of our problem is most likely be as same as $\Pi_{i=1}^{10} 1.103331113225399 e^{-0.2x_i}$.

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_64.pdf}
    \caption{importance sampling on CPU ($N=64$)}
    \label{fig:import_cpu_64}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_128.pdf}
    \caption{importance sampling on CPU ($N=128$)}
    \label{fig:import_cpu_128}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_256.pdf}
    \caption{importance sampling on CPU ($N=256$)}
    \label{fig:import_cpu_256}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_512.pdf}
    \caption{importance sampling on CPU ($N=512$)}
    \label{fig:import_cpu_512}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_1024.pdf}
    \caption{importance sampling on CPU ($N=1024$)}
    \label{fig:import_cpu_1024}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_2048.pdf}
    \caption{importance sampling on CPU ($N=2048$)}
    \label{fig:import_cpu_2048}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_4096.pdf}
    \caption{importance sampling on CPU ($N=4096$)}
    \label{fig:import_cpu_4096}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_8192.pdf}
    \caption{importance sampling on CPU ($N=8192$)}
    \label{fig:import_cpu_8192}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_16384.pdf}
    \caption{importance sampling on CPU ($N=16384$)}
    \label{fig:import_cpu_16384}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_32768.pdf}
    \caption{importance sampling on CPU ($N=32768$)}
    \label{fig:import_cpu_32768}
\end{figure}

\begin{figure}[H]
    \center
    \includegraphics[width=\textwidth]{import_cpu_65536.pdf}
    \caption{importance sampling on CPU ($N=65536$)}
    \label{fig:import_cpu_65536}
\end{figure}

\newpage

\section{GPU accelerated Monte Carlo integration in 10‐dimensions}

To optimize the process, we could use GPU to accelerated it. Due to the simplicity of \textbf{simple sampling}, we can naively parallelize our sampling process.
In order to get random numbers in GPU, I've used \texttt{cuRAND} as pseudo random number generator.

After performing some experiment I found that this improvement is quite large. 
Take $n=65536$ for example, the time of CPU method is $11.263$, and the GPU accelerated one is $0.311232$, which is abount $36$ times faster than the CPU one. Also, the error is very close, so this is a good taks to use GPU to accelerate it.

From figure \ref{fig:simple_gpu_cproc}, we can see that with larger grid size the CPU time used might slightly increase.
Also, from figure \ref{fig:simple_gpu_input} and figure \ref{fig:simple_gpu_gproc}, we knows that with larger grid size and block size the time might increase and, therefore, the bottleneck will become input time of GPU.
We can know the optimal block size is $256$ and grid size is $64$ in this task.

\begin{figure}[ht]
    \center
    \includegraphics[width=.85\textwidth]{simple_gpu_65536_0.pdf}
    \caption{total time of simple sampling on GPU ($N=65536$)}
    \label{fig:simple_gpu_total}
\end{figure}

\begin{figure}[ht]
    \center
    \includegraphics[width=.85\textwidth]{simple_gpu_65536_1.pdf}
    \caption{input time of simple sampling on GPU ($N=65536$)}
    \label{fig:simple_gpu_input}
\end{figure}

\begin{figure}[ht]
    \center
    \includegraphics[width=.85\textwidth]{simple_gpu_65536_2.pdf}
    \caption{gpu process time of simple sampling on GPU ($N=65536$)}
    \label{fig:simple_gpu_gproc}
\end{figure}

\begin{figure}[ht]
    \center
    \includegraphics[width=.85\textwidth]{simple_gpu_65536_3.pdf}
    \caption{cpu process time of simple sampling on GPU ($N=65536$)}
    \label{fig:simple_gpu_cproc}
\end{figure}

For \textbf{importance sampling}, we can know that the optimal argument for weight function will be as same as what CPU does.
However, this is not an easy job to parallelize our task because the way we generate random number is a sequential method which means it depends on the previous output to decide what to do now.
After discussing with TA, I've made a single Markov Chain for each GPU thread.
However, this method will increase the error because Markov Chain converge as the length increase, but we can't have such a long length in our parallelize method.
Nonetheless, I believe with larger $N$, this method on GPU might get less error and has incredible speed up.

Below are some figures about this task with \textbf{importance sampling}.
We can see that there is no significant difference between performance distribution of \textbf{simple sampling} and \textbf{importance sampling}, but the error here is $0.000226030396642$ while the CPU one is $0.00007.3662811317$ and the GPU one \textbf{simple sampling} is $0.000226585103697$.
But the speed up is larger than what \textbf{simple sampling} did, which is about $85$ times faster here.
Therefore, I guess that this method will perform pretty well with larger $N$.

\begin{figure}[ht]
    \center
    \includegraphics[width=.85\textwidth]{import_gpu_65536_0.pdf}
    \caption{total time of simple importance on GPU ($N=65536$)}
    \label{fig:import_gpu_total}
\end{figure}

\begin{figure}[ht]
    \center
    \includegraphics[width=.85\textwidth]{import_gpu_65536_1.pdf}
    \caption{input time of simple importance on GPU ($N=65536$)}
    \label{fig:import_gpu_input}
\end{figure}

\begin{figure}[ht]
    \center
    \includegraphics[width=.85\textwidth]{import_gpu_65536_2.pdf}
    \caption{gpu process time of importance sampling on GPU ($N=65536$)}
    \label{fig:import_gpu_gproc}
\end{figure}

\begin{figure}[ht]
    \center
    \includegraphics[width=.85\textwidth]{import_gpu_65536_3.pdf}
    \caption{cpu process time of importance sampling on GPU ($N=65536$)}
    \label{fig:import_gpu_cproc}
\end{figure}

\end{document}
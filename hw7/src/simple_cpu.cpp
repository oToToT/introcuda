#include <chrono>
#include <random>
#include <vector>
#include <utility>
#include <tuple>
#include <cmath>
#include <cstdio>
using llf = double;

llf f(const std::vector<llf>& v) {
    llf rep = 1;
    for (auto x: v) rep += x * x;
    return 1 / rep;
}

std::pair<llf, llf> solve(int n, std::mt19937& rnd) {
    llf tot = 0, tot2 = 0;
    std::uniform_real_distribution<llf> sampler(0, 1);
    for (int i = 0; i < n; ++i) {
        std::vector<llf> v(10);
        for (llf& x: v) x = sampler(rnd);
        llf y = f(v);
        tot += y, tot2 += y * y;
    }
    tot /= n, tot2 /= n;
    return {tot, sqrt(tot2 - tot * tot) / sqrt(n)};
}

int main() {
    std::mt19937 rnd(7122);
    
    puts("Enter the number of sampling: ");
    int n; scanf("%d", &n);
    printf("%d\n", n);
 
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
   
    llf mean, std;
    std::tie(mean, std) = solve(n, rnd);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    uint64_t t = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();

    printf("Processing time: %lfms \n", static_cast<llf>(t) / 1000);

    printf("%.15lf +- %.15lf\n", mean, std);
    return 0;
}

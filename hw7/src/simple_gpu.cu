#include <iostream>
#include <iomanip>
#include <random>
#include <curand_kernel.h>
typedef double llf;
#define DIM 10

__device__ llf f(llf v[]) {
    llf rep = 1;
    for (int i = 0; i < DIM; ++i)
        rep += v[i] * v[i];
    return 1 / rep;
}

__global__ void solve(int n, llf tot[], llf tot2[]) {
    extern __shared__ llf shmem[];

    size_t i = threadIdx.x + (blockIdx.x * blockDim.x);
    const int tid = threadIdx.x;
    const int blockSize = blockDim.x;

    curandState state;
    curand_init(i, 0, 0, &state);
    
    llf rnv[DIM], t1 = 0, t2 = 0;

    while (i < n) {
        for (int j = 0; j < DIM; ++j)
            rnv[j] = curand_uniform_double(&state);
        llf y = f(rnv);
        t1 += y, t2 += y * y;

        i += gridDim.x * blockDim.x;
    }
    shmem[tid] = t1;
    shmem[tid + blockSize] = t2;

    for (int step = blockSize >> 1; step; step >>= 1) {
        __syncthreads();
        if (tid < step) {
            shmem[tid] += shmem[tid + step];
            shmem[tid + blockSize] += shmem[tid + blockSize + step];
        }
    }

    if (tid == 0) {
        tot[blockIdx.x] = shmem[0];
        tot2[blockIdx.x] = shmem[blockSize];
    }
}

int main(int argc, char* argv[]) {
    if (argc < 4) {
        printf("Usage: %s GPU_ID BLOCK_SIZE GRID_SIZE\n", argv[0]);
        exit(0);
    }

    int gid = atoi(argv[1]);
    if (cudaSetDevice(gid) != cudaSuccess) {
        printf("Error using device %d\n", gid);
        exit(0);
    }
    printf("Using device %d\n", gid);

    int blockSize = atoi(argv[2]);
    if (blockSize > 1024) {
        puts("The block size must be less than 1024!");
        exit(0);
    }
    printf("The block size is %d\n", blockSize);

    int gridSize = atoi(argv[3]);
    printf("The grid size is %d\n", gridSize);


    puts("Enter the number of sampling: ");
    int n; scanf("%d", &n);
    printf("%d\n", n);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // allocate memory
    cudaEventRecord(start, 0);

    llf *tot = NULL, *tot2 = NULL;
    cudaMallocManaged(&tot, gridSize * sizeof(llf));
    cudaMallocManaged(&tot2, gridSize * sizeof(llf));

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float Intime;
    cudaEventElapsedTime(&Intime, start, stop);
    printf("Input time for GPU: %f (ms) \n", Intime);


    cudaEventRecord(start, 0);

    int sm = blockSize * sizeof(llf);
    solve<<<gridSize, blockSize, sm * 2>>>(n, tot, tot2);

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float gputime;
    cudaEventElapsedTime(&gputime, start, stop);
    printf("Processing time for GPU: %f (ms) \n", gputime);


    cudaEventRecord(start, 0);
    llf tot_v = 0, tot2_v = 0;

    for (int i = 0; i < gridSize; ++i) {
        tot_v += tot[i];
        tot2_v += tot2[i];
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float Calctime;
    cudaEventElapsedTime(&Calctime, start, stop);
    printf("Calculation time in CPU: %f (ms)\n", Calctime);

    llf mean = tot_v / n;
    llf err = tot2_v / n;
    err = sqrt(err - mean * mean) / sqrt(n);

    printf("%.15lf +- %.15lf\n", mean, err);
    return 0;
}

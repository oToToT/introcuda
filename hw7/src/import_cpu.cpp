#include <chrono>
#include <random>
#include <vector>
#include <utility>
#include <tuple>
#include <cmath>
#include <cstdio>
using llf = double;

llf C, a;

llf f(const std::vector<llf>& v) {
    llf rep = 1;
    for (auto x: v) rep += x * x;
    return 1 / rep;
}

llf w(llf x) {
    return C * exp(-a * x);
}
llf W(const std::vector<llf>& v) {
    llf r = 1;
    for (llf x : v) r *= w(x);
    return r;
}

llf importance_sampling(std::mt19937& rnd, std::vector<llf>& v) {
    static std::vector<llf> old(10);
    static llf w_old;
    static bool init = true;
    static std::uniform_real_distribution<llf> uni(0, 1);
    if (init) {
        for (llf &x: old) x = uni(rnd);
        w_old = W(old);
        init = false;
    }
    for (llf &x: v) x = uni(rnd);
    llf w_new = W(v);
    if (w_new >= w_old) {
        old = v;
        w_old = w_new;
    } else {
        llf r = uni(rnd);
        if (r < w_new / w_old) {
            old = v;
            w_old = w_new;
        }
    }
    v = old;
    return w_old;
}

std::pair<llf, llf> solve(int n, std::mt19937& rnd) {
    llf tot = 0, tot2 = 0;
    std::vector<llf> v(10);
    for (int i = 0; i < n; ++i) {
        llf w = importance_sampling(rnd, v);
        llf y = f(v) / w;
        tot += y, tot2 += y * y;
    }
    tot /= n, tot2 /= n;
    return {tot, sqrt(tot2 - tot * tot) / sqrt(n)};
}

int main(int argc, char* argv[]) {
    // -C/a e^{-a} - C = 1
    C = 1, a = 0;
    if (argc == 3) {
        C = atof(argv[1]);
        a = atof(argv[2]);
    }

    puts("Enter the number of sampling: ");
    int n; scanf("%d", &n);
    printf("%d\n", n);

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    std::mt19937 rnd(7122);
    llf mean, std;
    std::tie(mean, std) = solve(n, rnd);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    uint64_t t = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();

    printf("Processing time: %lfms \n", static_cast<llf>(t) / 1000);

    printf("%.15lf +- %.15lf\n", mean, std);
    return 0;
}

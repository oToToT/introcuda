Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 2D lattice with boundary conditions
Enter the size of the square lattice: 512 512
Enter the number of threads (tx,ty) per block: 8 8
The dimension of the grid is (64, 64)
Input time for GPU: 1.020608 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 428370
Processing time for GPU: 38761.414062 (ms) 
GPU Gflops: 20.121383
Output time for GPU: 0.957824 (ms) 

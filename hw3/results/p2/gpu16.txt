Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 2D lattice with boundary conditions
Enter the size of the square lattice: 512 512
Enter the number of threads (tx,ty) per block: 16 16
The dimension of the grid is (32, 32)
Input time for GPU: 1.031584 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 428370
Processing time for GPU: 49807.101562 (ms) 
GPU Gflops: 15.659077
Output time for GPU: 0.934784 (ms) 

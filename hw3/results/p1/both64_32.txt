Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 2D lattice with boundary conditions
Enter the size (Nx, Ny) of the 2D lattice: 64 64
Enter the number of threads (tx,ty) per block: 32 32
The dimension of the grid is (2, 2)
To compute the solution vector with CPU/GPU/both (0/1/2) ? 2

Input time for GPU: 0.157504 (ms) 
error (GPU) = 5.820766091346741e-11
total iterations (GPU) = 9746
Processing time for GPU: 209.758011 (ms) 
GPU Gflops: 1.250228
Output time for GPU: 0.119616 (ms) 
Total time for GPU: 210.035126 (ms) 

error (CPU) = 5.820766091346741e-11
total iterations (CPU) = 9745
Processing time for CPU: 138.236740 (ms) 
CPU Gflops: 1.896880
Speed up of GPU = 0.658160


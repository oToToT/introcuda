Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 2D lattice with boundary conditions (using texture)
Enter the size (Nx, Ny) of the 2D lattice: 32 32
Enter the number of threads (tx,ty) per block: 4 4
The dimension of the grid is (8, 8)
To compute the solution vector with CPU (1/0) ?0
Input time for GPU: 0.154560 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 2606
Processing time for GPU: 50.204544 (ms) 
GPU Gflops: 0.327018
Output time for GPU: 0.116096 (ms) 
Total time for GPU: 50.475201 (ms) 


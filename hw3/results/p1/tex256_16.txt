Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 2D lattice with boundary conditions (using texture)
Enter the size (Nx, Ny) of the 2D lattice: 256 256
Enter the number of threads (tx,ty) per block: 16 16
The dimension of the grid is (16, 16)
To compute the solution vector with CPU (1/0) ?0
Input time for GPU: 0.330048 (ms) 
error (GPU) = 5.820766091346741e-11
total iterations (GPU) = 124611
Processing time for GPU: 2961.938232 (ms) 
GPU Gflops: 18.999661
Output time for GPU: 0.296576 (ms) 
Total time for GPU: 2962.564941 (ms) 


Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 2D lattice with boundary conditions
Enter the size (Nx, Ny) of the 2D lattice: 32 32
Enter the number of threads (tx,ty) per block: 8 8
The dimension of the grid is (4, 4)
To compute the solution vector with CPU/GPU/both (0/1/2) ? 2

Input time for GPU: 0.194528 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 2606
Processing time for GPU: 51.557377 (ms) 
GPU Gflops: 0.318437
Output time for GPU: 0.138912 (ms) 
Total time for GPU: 51.890816 (ms) 

error (CPU) = 0.000000000000000e+00
total iterations (CPU) = 2606
Processing time for CPU: 14.291776 (ms) 
CPU Gflops: 1.148759
Speed up of GPU = 0.275420


Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 2D lattice with boundary conditions (using texture)
Enter the size (Nx, Ny) of the 2D lattice: 128 128
Enter the number of threads (tx,ty) per block: 16 16
The dimension of the grid is (8, 8)
To compute the solution vector with CPU (1/0) ?0
Input time for GPU: 0.225344 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 35073
Processing time for GPU: 801.509949 (ms) 
GPU Gflops: 4.862987
Output time for GPU: 0.152832 (ms) 
Total time for GPU: 801.888123 (ms) 


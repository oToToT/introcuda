Solve Laplace equation on a 3D lattice with boundary conditions
Enter the size of the square lattice: 8 8 8
error = 9.072905707979695e-11
total iterations = 97
Processing time for CPU: 0.096576 (ms) 
CPU Gflops: 1.952535

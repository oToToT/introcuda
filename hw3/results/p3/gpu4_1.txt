Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 3D lattice with boundary conditions
Enter the size of the square lattice: 4 4 4
Enter the number of threads (tx,ty,tz) per block: 1 1 1
The dimension of the grid is (4, 4, 4)

Initial field configuration:1.00e+00 1.00e+00 1.00e+00 1.00e+00
1.00e+00 1.00e+00 1.00e+00 1.00e+00
1.00e+00 1.00e+00 1.00e+00 1.00e+00
1.00e+00 1.00e+00 1.00e+00 1.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
Input time for GPU: 0.150240 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 25
Processing time for GPU: 0.357120 (ms) 
GPU Gflops: 0.005040
Output time for GPU: 0.107904 (ms) 

Final field configuration:1.00e+00 1.00e+00 1.00e+00 1.00e+00
1.00e+00 1.00e+00 1.00e+00 1.00e+00
1.00e+00 1.00e+00 1.00e+00 1.00e+00
1.00e+00 1.00e+00 1.00e+00 1.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 2.67e-01 2.67e-01 0.00e+00
0.00e+00 2.67e-01 2.67e-01 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 6.67e-02 6.67e-02 0.00e+00
0.00e+00 6.67e-02 6.67e-02 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00
0.00e+00 0.00e+00 0.00e+00 0.00e+00

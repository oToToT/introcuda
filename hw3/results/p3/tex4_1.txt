Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 3D lattice with boundary conditions
Enter the size of the square lattice: 4 4 4
Enter the number of threads (tx,ty,tz) per block: 1 1 1
The dimension of the grid is (4, 4, 4)
Input time for GPU: 0.157632 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 25
Processing time for GPU: 0.401824 (ms) 
GPU Gflops: 0.004480
Output time for GPU: 0.109024 (ms) 

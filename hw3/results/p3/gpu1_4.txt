Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 3D lattice with boundary conditions
Enter the size of the square lattice: 1 1 1
Enter the number of threads (tx,ty,tz) per block: 4 4 4
The block size in x is incorrect

Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 3D lattice with boundary conditions
Enter the size of the square lattice: 4 4 4
Enter the number of threads (tx,ty,tz) per block: 4 4 4
The dimension of the grid is (1, 1, 1)
Input time for GPU: 0.155936 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 25
Processing time for GPU: 0.579680 (ms) 
GPU Gflops: 0.003105
Output time for GPU: 0.108352 (ms) 

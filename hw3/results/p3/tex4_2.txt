Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 3D lattice with boundary conditions
Enter the size of the square lattice: 4 4 4
Enter the number of threads (tx,ty,tz) per block: 2 2 2
The dimension of the grid is (2, 2, 2)
Input time for GPU: 0.158272 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 25
Processing time for GPU: 0.400896 (ms) 
GPU Gflops: 0.004490
Output time for GPU: 0.108640 (ms) 

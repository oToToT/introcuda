Enter the GPU ID (0/1): 1
Select GPU with device ID = 1
Solve Laplace equation on a 3D lattice with boundary conditions
Enter the size of the square lattice: 2 2 2
Enter the number of threads (tx,ty,tz) per block: 1 1 1
The dimension of the grid is (2, 2, 2)

Initial field configuration:1.00e+00 1.00e+00
1.00e+00 1.00e+00
0.00e+00 0.00e+00
0.00e+00 0.00e+00
Input time for GPU: 0.148128 (ms) 
error (GPU) = 0.000000000000000e+00
total iterations (GPU) = 1
Processing time for GPU: 0.033312 (ms) 
GPU Gflops: 0.000000
Output time for GPU: 0.104800 (ms) 

Final field configuration:1.00e+00 1.00e+00
1.00e+00 1.00e+00
0.00e+00 0.00e+00
0.00e+00 0.00e+00

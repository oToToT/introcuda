#include <stdio.h>

void initialize_lattice_size(int *x, int *y) {
    puts("Solve Laplace equation on a 2D lattice with boundary conditions");
    printf("Enter the size of the square lattice: ");
    scanf("%d %d", x, y);
    printf("%d %d\n", *x, *y);
}
void initialize_lattice(int x, int y, float **h) {
    const int size = x * y * sizeof(float); 
    *h = (float*)malloc(size);
    memset(*h, 0, size);
    /* top=1, left=-1, right=-2, bottom=5 */
    for (int j = 0; j < y; ++j)
        (*h)[0 * y + j] = 1;
    for (int i = 0; i < x; ++i)
        (*h)[i * y + 0] = -1;
    for (int i = 0; i < x; ++i)
        (*h)[i * y + (y-1)] = -2;
    for (int j = 0; j < y; ++j)
        (*h)[(x - 1) * y + j] = 5;
}
void save_field_config(int x, int y, float *h, const char *fn, const char *state) {
    FILE *fp;
    fp = fopen(fn, "w");
    fprintf(fp, "%s field configuration:\n", state);
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; ++j) {
            if (j) fputc(' ', fp);
            fprintf(fp, "%.2e", h[i * y + j]);
        }
        fputc('\n', fp);
    }
    fclose(fp);

    //printf("\n%s field configuration:\n", state);
    //for (int i = 0; i < x; ++i) {
        //for (int j = 0; j < y; ++j) {
            //if (j) putchar(' ');
            //printf("%.2e", h[i * y + j]);
        //}
        //putchar('\n');
    //}
}

int solve_laplace(int x, int y, float *h[2], int iter, double eps) {
    int which = 0, it;
    double error = 10 * eps;
    for (it = 0; it < iter && error > eps; ++it) {
        error = 0;
        for (int i = 1; i < x - 1; ++i) {
            for (int j = 1; j < y - 1; ++j) {
                float up = h[which][(i - 1) * y + j];
                float left = h[which][i * y + (j - 1)];
                float down = h[which][(i + 1) * y + j];
                float right = h[which][i * y + (j + 1)];
                h[which ^ 1][i * y + j] = (up + left + down + right) / 4;
                double diff = h[which ^ 1][i * y + j] - h[which][i * y + j];
                error += diff * diff;
            }
        }
        which ^= 1;
    }
    printf("error = %.15e\n", error);
    printf("total iterations = %d\n", it);
    return it;
}

int main() {
    int Nx, Ny;
    initialize_lattice_size(&Nx, &Ny);
    float *h[2];
    initialize_lattice(Nx, Ny, &h[0]);
    initialize_lattice(Nx, Ny, &h[1]);
    save_field_config(Nx, Ny, h[0], "phi_initial.dat", "Initial");

    // create the timer
    cudaEvent_t start,stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    //start the timer
    cudaEventRecord(start, 0);
    int iter = solve_laplace(Nx, Ny, h, 1000000, 1e-10);
    // stop the timer
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
   
    float cputime;
    cudaEventElapsedTime(&cputime, start, stop);
    printf("Processing time for CPU: %f (ms) \n", cputime);
    double flops = 7.0 * (Nx-2) * (Ny-2) * iter;
    printf("CPU Gflops: %lf\n", flops / (1000000.0 * cputime));
    
    // destroy the timer
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    save_field_config(Nx, Ny, h[0], "phi_CPU.dat", "Final");

    free(h[0]);
    free(h[1]);
    cudaDeviceReset();
    return 0;
}

#include <stdio.h>
#include <math.h>

texture<float> tex0;
texture<float> tex1;

__global__ void laplacian(float *device0, float *device1, float *C, bool flag) {
    texture<float> tex[] = {tex0, tex1};
    float *devices[] = {device0, device1};
    extern __shared__ float cache[];

    const int Nx = blockDim.x * gridDim.x;
    const int Ny = blockDim.y * gridDim.y;
    const int Nz = blockDim.z * gridDim.z;
    const int i = blockDim.x * blockIdx.x + threadIdx.x;
    const int j = blockDim.y * blockIdx.y + threadIdx.y;
    const int k = blockDim.z * blockIdx.z + threadIdx.z;
    const int cacheIdx = threadIdx.x * blockDim.y * blockDim.z + threadIdx.y * blockDim.z + threadIdx.z;
    const int blockId = blockIdx.x * gridDim.y * gridDim.z + blockIdx.y * gridDim.z + blockIdx.z;
    const int bN = blockDim.x * blockDim.y * blockDim.z;

    float diff = 0;
    if (i && i != Nx-1 && j && j != Ny-1 && k && k != Nz-1) {
        const float a = tex1Dfetch(tex[flag], (i - 1) * Ny * Nz + j * Nz + k);
        const float c = tex1Dfetch(tex[flag], (i + 1) * Ny * Nz + j * Nz + k);
        const float b = tex1Dfetch(tex[flag], i * Ny * Nz + (j - 1) * Nz + k);
        const float d = tex1Dfetch(tex[flag], i * Ny * Nz + (j + 1) * Nz + k);
        const float e = tex1Dfetch(tex[flag], i * Ny * Nz + j * Nz + (k - 1));
        const float f = tex1Dfetch(tex[flag], i * Ny * Nz + j * Nz + (k + 1));
        const float o = tex1Dfetch(tex[flag], i * Ny * Nz + j * Nz + k);
        devices[flag ^ 1][i * Ny * Nz + j * Nz + k] = (a + b + c + d + e + f) / 6;
        diff = devices[flag ^ 1][i * Ny * Nz + j * Nz + k] - o;
    }

    cache[cacheIdx] = diff * diff;
    for (int step = bN >> 1; step; step >>= 1) {
        __syncthreads();
        if (cacheIdx < step) {
            cache[cacheIdx] += cache[cacheIdx + step];
        }
    }
    if (cacheIdx == 0) C[blockId] = cache[0];
}

int initialize_gpu() {
    int gid;
    printf("Enter the GPU ID (0/1): ");
    scanf("%d", &gid);
    printf("%d\n", gid);

    // Error code to check return values for CUDA calls
    cudaError_t err = cudaSuccess;
    err = cudaSetDevice(gid);
    if (err != cudaSuccess) {
        printf("!!! Cannot select GPU with device ID = %d\n", gid);
        exit(1);
    }
    printf("Select GPU with device ID = %d\n", gid);

    return gid;
}
void initialize_lattice_size(int *x, int *y, int *z) {
    puts("Solve Laplace equation on a 3D lattice with boundary conditions");
    printf("Enter the size of the square lattice: ");
    scanf("%d %d %d", x, y, z);
    printf("%d %d %d\n", *x, *y, *z);
}
void initialize_lattice(int x, int y, int z, float **h) {
    const int size = x * y * z * sizeof(float); 
    *h = (float*)malloc(size);
    memset(*h, 0, size);
    for (int j = 0; j < y; ++j)
        for (int k = 0; k < z; ++k)
            (*h)[j * z + k] = 1;
}
void save_field_config(int x, int y, int z, float *h, const char *fn, const char *state) {
    FILE *fp;
    fp = fopen(fn, "w");
    fprintf(fp, "%s field configuration:\n", state);
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; ++j) {
            for (int k = 0; k < z; ++k) {
                if (k) fputc(' ', fp);
                fprintf(fp, "%.2e", h[i * y * z + j * z + k]);
            }
            fputc('\n', fp);
        }
    }
    fclose(fp);

    //printf("\n%s field configuration:\n", state);
    //for (int i = 0; i < x; ++i) {
        //for (int j = 0; j < y; ++j) {
            //for (int k = 0; k < z; ++k) {
                //if (k) putchar(' ');
                //printf("%.2e", h[i * y * z + j * z + k]);
            //}
            //putchar('\n');
        //}
    //}
}
void retrieve_block_size(int *tx, int *ty, int *tz) {
    printf("Enter the number of threads (tx,ty,tz) per block: ");
    scanf("%d %d %d", tx, ty, tz);
    printf("%d %d %d\n", *tx, *ty, *tz);
    if ((*tx) * (*ty) * (*tz) > 1024) {
        printf("The number of threads per block must be less than 1024 ! \n");
        exit(0);
    }
}

int main() {
    const int ITER_MAX = 1000000;
    const float eps = 1e-10;
    int gid = initialize_gpu();
    int Nx, Ny, Nz;
    initialize_lattice_size(&Nx, &Ny, &Nz);
    int tx, ty, tz;
    retrieve_block_size(&tx, &ty, &tz);
    dim3 threads(tx, ty, tz);
    int bx = Nx/tx;
    if (bx*tx != Nx) {
        printf("The block size in x is incorrect\n"); 
        exit(0);
    }
    int by = Ny/ty;
    if (by*ty != Ny) {
        printf("The block size in y is incorrect\n"); 
        exit(0);
    }
    int bz = Nz/tz;
    if (bz*tz != Nz) {
        printf("The block size in z is incorrect\n"); 
        exit(0);
    }
    if (bx > 65535 || by > 65535 || bz > 65535) {
        printf("The grid size exceeds the limit ! \n");
        exit(0);
    }
    dim3 blocks(bx, by, bz);
    printf("The dimension of the grid is (%d, %d, %d)\n", bx, by, bz);

    float *h, *g, *hostC;
    initialize_lattice(Nx, Ny, Nz, &h);
    save_field_config(Nx, Ny, Nz, h, "phi_initial.dat", "Initial");

    int size = Nx * Ny * Nz * sizeof(float);
    int sb = bx * by * bz * sizeof(float);
    int sm = tx * ty * tz * sizeof(float);
    hostC = (float*)malloc(sb);
    g = (float*)malloc(size);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    float *device0, *device1, *deviceC;
    // start the timer
    cudaEventRecord(start,0);
    cudaMalloc((void**)&device0, size);
    cudaMalloc((void**)&device1, size);
    cudaMalloc((void**)&deviceC, sb);
    cudaBindTexture(NULL, tex0, device0, size);
    cudaBindTexture(NULL, tex1, device1, size);
    cudaMemcpy(device0, h, size, cudaMemcpyHostToDevice);
    cudaMemcpy(device1, h, size, cudaMemcpyHostToDevice);
    // stop the timer
    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);
    float Intime;
    cudaEventElapsedTime(&Intime, start, stop);
    printf("Input time for GPU: %f (ms) \n", Intime);

    // start the timer
    cudaEventRecord(start, 0);
    float err = 10 * eps;
    int which = 0, iter;
    for (iter = 0; iter < ITER_MAX && err > eps; ++iter) {
        laplacian<<<blocks,threads,sm>>>(device0, device1, deviceC, which);
        cudaMemcpy(hostC, deviceC, sb, cudaMemcpyDeviceToHost);
        err = 0.0;
        for (int i = 0; i < bx * by * bz; ++i)
            err += hostC[i];
        err = sqrt(err);

        which ^= 1;
    }
    printf("error (GPU) = %.15e\n", err);
    printf("total iterations (GPU) = %d\n", iter);

    // stop the timer
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float cputime;
    cudaEventElapsedTime(&cputime, start, stop);
    printf("Processing time for GPU: %f (ms) \n", cputime);
    double flops = 9.0 * (Nx-2) * (Ny-2) * (Nz-2) * iter;
    printf("GPU Gflops: %lf\n", flops / (1000000.0 * cputime));

    // start the timer
    cudaEventRecord(start, 0);

    cudaMemcpy(g, device0, size, cudaMemcpyDeviceToHost);

    cudaFree(device0);
    cudaFree(device1);
    cudaFree(deviceC);
    cudaUnbindTexture(tex0);
    cudaUnbindTexture(tex1);

    // stop the timer
    cudaEventRecord(stop,0);
    cudaEventSynchronize(stop);

    float Outime;
    cudaEventElapsedTime(&Outime, start, stop);
    printf("Output time for GPU: %f (ms) \n",Outime);

    // destroy the timer
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    save_field_config(Nx, Ny, Nz, g, "phi_GPU.dat", "Final");

    free(h);
    free(g);
    free(hostC);
    cudaDeviceReset();
    return 0;
}

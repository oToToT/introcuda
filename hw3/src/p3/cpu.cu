#include <stdio.h>

void initialize_lattice_size(int *x, int *y, int *z) {
    puts("Solve Laplace equation on a 3D lattice with boundary conditions");
    printf("Enter the size of the square lattice: ");
    scanf("%d %d %d", x, y, z);
    printf("%d %d %d\n", *x, *y, *z);
}
void initialize_lattice(int x, int y, int z, float **h) {
    const int size = x * y * z * sizeof(float); 
    *h = (float*)malloc(size);
    memset(*h, 0, size);
    for (int j = 0; j < y; ++j)
        for (int k = 0; k < z; ++k)
            (*h)[j * z + k] = 1;
}
void save_field_config(int x, int y, int z, float *h, const char *fn, const char *state) {
    FILE *fp;
    fp = fopen(fn, "w");
    fprintf(fp, "%s field configuration:", state);
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; ++j) {
            for (int k = 0; k < z; ++k) {
                if (k) fputc(' ', fp);
                fprintf(fp, "%.2e", h[i * y * z + j * z + k]);
            }
            fputc('\n', fp);
        }
    }
    fclose(fp);

    //printf("\n%s field configuration:", state);
    //for (int i = 0; i < x; ++i) {
        //for (int j = 0; j < y; ++j) {
            //for (int k = 0; k < z; ++k) {
                //if (k) putchar(' ');
                //printf("%.2e", h[i * y * z + j * z + k]);
            //}
            //putchar('\n');
        //}
    //}
}

int solve_laplace(int x, int y, int z, float *h[2], int iter, double eps) {
    int which = 0, it;
    double error = 10 * eps;
    for (it = 0; it < iter && error > eps; ++it) {
        error = 0;
        for (int i = 1; i < x - 1; ++i) {
            for (int j = 1; j < y - 1; ++j) {
                for (int k = 1; k < z - 1; ++k) {
                    float a = h[which][(i - 1) * y * z + j * z + k];
                    float c = h[which][(i + 1) * y * z + j * z + k];
                    float b = h[which][i * y * z + (j - 1) * z + k];
                    float d = h[which][i * y * z + (j + 1) * z + k];
                    float e = h[which][i * y * z + j * z + (k - 1)];
                    float f = h[which][i * y * z + j * z + (k + 1)];
                    h[which ^ 1][i * y * z + j * z + k] = (a + b + c + d + e + f) / 6;
                    double diff = h[which ^ 1][i * y * z + j * z + k] - h[which][i * y * z + j * z + k];
                    error += diff * diff;
                }
            }
        }
        which ^= 1;
    }
    printf("error = %.15e\n", error);
    printf("total iterations = %d\n", it);
    return it;
}

int main() {
    int Nx, Ny, Nz;
    initialize_lattice_size(&Nx, &Ny, &Nz);
    float *h[2];
    initialize_lattice(Nx, Ny, Nz, &h[0]);
    initialize_lattice(Nx, Ny, Nz, &h[1]);
    save_field_config(Nx, Ny, Nz, h[0], "phi_initial.dat", "Initial");

    // create the timer
    cudaEvent_t start,stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    //start the timer
    cudaEventRecord(start, 0);
    int iter = solve_laplace(Nx, Ny, Nz, h, 100000000, 1e-10);
    // stop the timer
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
   
    float cputime;
    cudaEventElapsedTime(&cputime, start, stop);
    printf("Processing time for CPU: %f (ms) \n", cputime);
    double flops = 9.0 * (Nx-2) * (Ny-2) * (Nz-2) * iter;
    printf("CPU Gflops: %lf\n", flops / (1000000.0 * cputime));
    
    // destroy the timer
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    save_field_config(Nx, Ny, Nz, h[0], "phi_CPU.dat", "Final");

    free(h[0]);
    free(h[1]);
    cudaDeviceReset();
    return 0;
}

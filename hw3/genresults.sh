#!/bin/sh

for ((i=32;i<=256;i=i+i))
do
    for ((j=4;j<=32;j=j+j))
    do
        echo "p1 $i $j"
        printf "1\n$i $i\n$j $j\n2" | bin/p1/laplace > "results/p1/both${i}_${j}.txt"
        printf "1\n$i $i\n$j $j\n0" | bin/p1/laplaceTex > "results/p1/tex${i}_${j}.txt"
    done
done


echo "p2"
printf "1\n512 512\n32 32\n" | bin/p2/laplace > /dev/null
printf "512 512\n32 32\n" | bin/p2/laplace_cpu > /dev/null
printf "1\n512 512\n32 32\n" | bin/p2/laplaceTex > /dev/null
diff phi_CPU.dat phi_GPU.dat > results/p2/cpu_gpu.txt
diff phi_GPU.dat phi_GPU_tex.dat > results/p2/gpu_tex.txt
diff phi_GPU_tex.dat phi_CPU.dat > results/p2/tex_cpu.txt

for ((i=1;i<=32;i=i+i))
do
    echo "p2 $i"
    printf "1\n512 512\n$i $i\n" | bin/p2/laplace > "results/p2/gpu${i}.txt"
    printf "1\n512 512\n$i $i\n" | bin/p2/laplaceTex > "results/p2/tex${i}.txt"
done


for ((i=1;i<=8;i=i+i))
do
    for ((j=1;j<=8;j=j+j))
    do
        echo "p3 $i $j"
        printf "1\n$i $i $i\n$j $j $j\n" | bin/p3/laplace > "results/p3/gpu${i}_${j}.txt"
        printf "$i $i $i\n$j $j $j\n" | bin/p3/laplace_cpu > "results/p3/cpu${i}_${j}.txt"
        printf "1\n$i $i $i\n$j $j $j\n" | bin/p3/laplaceTex > "results/p3/tex${i}_${j}.txt"
    done
done
